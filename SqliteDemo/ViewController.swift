//
//  ViewController.swift
//  SqliteDemo
//
//  Created by EPITADMBP04 on 4/4/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import UIKit
import SQLite

class ViewController: UIViewController {
  
    var database : Connection!
    
    let usersTable = Table("users")
    let id = Expression<Int>("id")
    let name = Expression<String>("name")
    let email = Expression<String>("email")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent("users").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        } catch {
            print(error)
        }
    }

    @IBAction func createTable() {
        print("createTable Clicked")
        let createTable = self.usersTable.create { (table) in
            table.column(self.id, primaryKey: true)
            table.column(self.name)
            table.column(self.email, unique: true)
        }
        do {
            try self.database.run(createTable)
            print("Table Created")
        } catch {
            print(error)
        }
    }
    
    @IBAction func insertUsers() {
           let alert = UIAlertController(title: "Insert User", message: nil, preferredStyle: .alert)
               alert.addTextField { (tf) in tf.placeholder = "User ID" }
               alert.addTextField { (tf) in tf.placeholder = "Email" }
               let action = UIAlertAction(title: "Submit", style: .default) { (_) in
               guard let userName = alert.textFields?.first?.text, let userEmail = alert.textFields?.last?.text else { return }
                
                let insertUser = self.usersTable.insert(self.name <- userName, self.email <- userEmail)
                
                do {
                    try self.database.run(insertUser)
                    print("Inserted User")
                } catch {
                    print(error)
                  }
               }
               alert.addAction(action)
               present(alert, animated: true)
              }

    @IBAction func listUser() {
           print("ListUserClicked")
        do {
            let users = try self.database.prepare(self.usersTable)
            for user in users {
                print("userId: \(user[self.id]), name: \(user[self.name]), email: \(user[self.email])")
            }
        } catch {
            print(error)
          }
       }

    @IBAction func updateUser() {
        let alert = UIAlertController(title: "Update User", message: nil, preferredStyle: .alert)
        alert.addTextField { (tf) in tf.placeholder = "User ID" }
        alert.addTextField { (tf) in tf.placeholder = "Email" }
        let action = UIAlertAction(title: "Submit", style: .default) { (_) in
        guard let userIdString = alert.textFields?.first?.text,
        let userId = Int(userIdString),
        let email = alert.textFields?.last?.text else { return }
            
            let user = self.usersTable.filter(self.id == userId)
            let updateUser = user.update(self.email <- email)
            do {
                try self.database.run(updateUser)
            } catch {
                print(error)
            }
        }
        alert.addAction(action)
        present(alert, animated: true)
       }
    
    @IBAction func deleteUser() {
           let alert = UIAlertController(title: "Delete User", message: nil, preferredStyle: .alert)
                 alert.addTextField { (tf) in tf.placeholder = "User ID" }
                 let action = UIAlertAction(title: "Submit", style: .default) { (_) in
                 guard let userIdString = alert.textFields?.first?.text, let userId = Int(userIdString) else { return }
                    let user = self.usersTable.filter(self.id == userId)
                    let deleteUser = user.delete()
                    do {
                        try self.database.run(deleteUser)
                    } catch {
                        print(error)
                    }
                 }
                 alert.addAction(action)
                 present(alert, animated: true)
                }
       }

